# ovish #

[![Build Status](https://drone.io/bitbucket.org/cantico/ovish/status.png)](https://drone.io/bitbucket.org/cantico/ovish/latest)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/cantico/ovish/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/cantico/ovish/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/b/cantico/ovish/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/cantico/ovish/?branch=master)

Le fichier executable de l'application :
[ovish.phar](https://bitbucket.org/cantico/ovish/downloads/ovish.phar)



## Installation développeur ##

1. cloner le dépôt git
2. lancer "composer install" dans le dossier
3. faire un lien symbolique /usr/bin/ovish vers le fichier ~/git/ovish/bin/ovish


## Créer le .phar ##

Le .phar est créé en utilisant [box](https://github.com/box-project/box2).

```bash
cd ~/git/ovish
box build
```


## Commandes pour mettre a jour ovidentia ##

Mettre à jour un module sur un site :


```bash
#!bash

ovish install -r http://www.ovidentia.org/ widgets-1-0-58.zip
```


Mettre à jour un module sur une une liste de sites :


```bash
#!bash

ovish install -s http://www.cantico.fr/sites -c widgets-1-0-58.zip
```

le fichier contenant la liste des sites peut être locale ou une url http. Le fichier doit contenir une url par ligne. Utiliser -c pour que la commande demande une seule fois l'identifiant et mot de passe. Si cette option n'est pas utilisée, l'identifiant sera demandé pour chaque site.


## Lister les version installées ##

```bash
#!bash

ovish rlist -r http://www.cantico.fr/
```

comparer les versions entre deux sites :
```bash
#!bash
diff -y <(ovish rlist -r "http://nickname:password@www.cantico.fr/") <(ovish rlist -r "http://nickname:password@www.ovidentia.org/")
```