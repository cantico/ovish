<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Ovish\OviCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


use Ovidentia\Ovish\Util\Remote;
use Ovidentia\Ovish\Util\CsrfToken;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Http\Client;
use Sunra\PhpSimple\HtmlDomParser;
use Zend\Stdlib\Parameters;

/**
 * List addons on a remote server
 */
class InstallCommand extends RemoteCommand
{
    
    protected function configure()
    {
        $this
        ->setName('install')
        ->setDescription('install or update package')
        ->addArgument('archive', InputArgument::OPTIONAL, 'install an ovidentia zip archive');
    }
    
    
    
    
    /**
     * Upload zip archive
     */
    protected function archive($archive, Client $client, OutputInterface $output)
    {
        $csrfToken = new CsrfToken();
        $token = $csrfToken->get(
            $client,
            array('tg' => 'addons', 'idx' => 'upload')
        );
        
        
        $client->setEncType(Client::ENC_FORMDATA);
        
        $request = new Request();
        $request->setUri($client->getUri());
        $request->setMethod(Request::METHOD_POST);
        
        $post = new Parameters();
        $post->fromArray(
            array(
                'tg'  => 'addons',
                'idx' => 'requirements',
                'babCsrfProtect' => $token
            )
        );
        
        $request->setPost($post);
        $client->setRequest($request);
        $client->setFileUpload($archive, 'uploadf');
        
        $response = $client->dispatch($request);
        
        /*@var $response Response */
        
        if (!$response->isSuccess()) {
            throw new \RunTimeException(
                $response->getStatusCode()
                .' Failed to get prerequisits page (POST failed on tg=addons&idx=requirements)'
            );
        }
        
        $body = $response->getBody();
        $dom = HtmlDomParser::str_get_html($body);
        
        if (!isset($dom) || !$dom) {
            throw new \RuntimeException('Failed to get HTML from the response (tg=addons&idx=requirements)');
        }
        
        $input = $dom->find('input[name=tmpfile]', 0);
        
        if (!$input) {
            print $body;
            throw new \RuntimeException('Upload rejected by ovidentia, input[name=tmpfile] not found in html');
        }
        
        $tmp_file = $input->value;
        
        if (empty($tmp_file)) {
            throw new \RuntimeException('Failed to upload the package');
        }
        
        $client->resetParameters();
        
        // go to import frame
        // index.php?tg=addons&idx=import_frame&tmpfile=fbae48484e34c8628cc907a3a567ad21_widgets-1-0-53.zip&dlfile=
        
        $request = new Request();
        $request->setUri($client->getUri());
        $request->setMethod(Request::METHOD_GET);
        $request->getQuery()->set('tg', 'addons')->set('idx', 'import_frame')->set('tmpfile', $tmp_file);
        
        $response = $client->send($request);
        
        /*@var $response Response */
        
        if (!$response->isSuccess()) {
            throw new \RunTimeException('Failed to get install frame page');
        }
        
        $frame_content = strip_tags($response->getBody());
        // in ovidentia charset
        
        // if the package is an ovidentia version, the frame content redirect to update via javascript
        if (false !== strpos($frame_content, '?tg=version&idx=upgrade&iframe=1')) {
            $frame_content = $this->versionUpgrade($client);
        }
        
        $frame_content = trim($frame_content);
        $contentType = $response->getHeaders()->get('Content-Type');
        if (isset($contentType)) {
            if (false !== strpos(strtolower($contentType->getFieldValue()), 'iso-8859-15')) {
                $frame_content = utf8_encode($frame_content);
            }
        }
        
        $output->writeln($frame_content);
    }
    
    
    
    private function versionUpgrade(Client $client)
    {
        $request = new Request();
        $request->setUri($client->getUri());
        $request->setMethod(Request::METHOD_GET);
        $request->getQuery()->set('tg', 'version')->set('idx', 'upgrade')->set('iframe', '1');
        
        $response = $client->send($request);
        
        /*@var $response Response */
        
        if (!$response->isSuccess()) {
            throw new \RunTimeException('Failed to get version upgrade frame page');
        }
        
        return strip_tags($response->getBody());
    }
    
    
    
    /**
     * Execute command when logged in
     */
    protected function userExecute(Client $client, InputInterface $input, OutputInterface $output)
    {
        $uri = $client->getUri()->getHost().$client->getUri()->getPath();
        
        $output->writeln('install: '.$uri);
        
        if ($archive = $input->getArgument('archive')) {
            $this->archive($archive, $client, $output);
        }
        
    }
}
