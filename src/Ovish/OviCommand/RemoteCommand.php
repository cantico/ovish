<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Ovish\OviCommand;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


use Ovidentia\Ovish\Util\Remote;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Http\Client;

abstract class RemoteCommand extends Command
{
    
    public function __construct($name = null)
    {
        parent::__construct($name = null);
        
        $this->addOption(
            'remote',
            'r',
            InputOption::VALUE_REQUIRED,
            'site to login into'
        );
        
        
        $this->addOption(
            'sites',
            's',
            InputOption::VALUE_REQUIRED,
            'remote or local list of sites to login into'
        );
        
        
        $this->addOption(
            'same-credentials',
            'c',
            InputOption::VALUE_NONE,
            'Use same credentials for all sites'
        );
    }
    
    

    
    /**
     * Get the list of remote url to process
     *
     * @param InputInterface $input
     *
     * @return Array
     */
    private function getUriList(InputInterface $input)
    {
        $uri = $input->getOption('remote');
        $sites = $input->getOption('sites');
        
        $list = array();
        
        if (!empty($uri)) {
            $list[] = $uri;
        }
        
        if (!empty($sites)) {
            $contents = file_get_contents($sites);
            $arr = preg_split('/\R/m', trim($contents));
            foreach ($arr as $uri) {
                $line = trim($uri);
                
                if (empty($line)) {
                    continue;
                }
                
                // comments
                
                if ('#' === substr($line, 0, 1)) {
                    continue;
                }
                
                $list[] = $line;
            }
        }
        
        if (empty($list)) {
            throw new \RunTimeException('One of remote option or sites option is mandatory');
        }
        
        return $list;
    }
    
    
    /**
     * Execute command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $remote = null;
        

        foreach ($this->getUriList($input) as $uri) {
            $previous = (isset($remote)) ? $remote : null;
            
            $remote = new Remote($uri);
            
            if (isset($previous) && $input->getOption('same-credentials')) {
                $remote->setCredRemote($previous);
            }
            
            $client = $remote->loginOnCommand($this, $output);
            
            if (!isset($client)) {
                throw new \RunTimeException('Login failed');
            }
            
            $this->userExecute($client, $input, $output);
        }
    }
    
    
    /**
     * Execute command in authenticated context
     * @param Client    $client     HTTP client with valid session cookie
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    abstract protected function userExecute(Client $client, InputInterface $input, OutputInterface $output);
}
