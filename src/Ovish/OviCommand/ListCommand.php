<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Ovish\OviCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


use Ovidentia\Ovish\Util\Remote;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Http\Client;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * List addons on a remote server
 */
class ListCommand extends RemoteCommand
{
    
    protected function configure()
    {
        $this
        ->setName('rlist')
        ->setDescription('List remote installed packages');
    }
    
    
    /**
     * Get addons on one page
     */
    private function getIdx(Client $client, $idx)
    {
        $addons = array();
        
        $request = new Request();
        $request->setMethod(Request::METHOD_GET);
        $request->setUri($client->getUri());
        $request->getQuery()->set('tg', 'addons')->set('idx', $idx);
        
        $response = $client->send($request);
        
        /*@var $response Response */
        
        if (!$response->isSuccess()) {
            throw new \RunTimeException('Failed to get addons page');
        }
        
        $dom = HtmlDomParser::str_get_html($response->getBody());
        
        $links = $dom->find('tr.BabForumBackground1 strong a, tr.BabSiteAdminFontBackground strong a');
        foreach ($links as $link) {
            $tr = $link->parent()->parent()->parent();
            $td_version = $tr->find('td', 3);

            if (isset($link) && isset($link->plaintext) && isset($td_version)) {
                $addons[$link->plaintext] = $td_version->plaintext;
            }
            
        }
        
        return $addons;
    }
    
    
    private function getCoreVersion(Client $client)
    {
        $request = new Request();
        $request->setMethod(Request::METHOD_GET);
        $request->setUri($client->getUri());
        $request->getQuery()->set('tg', 'version');
        
        $response = $client->send($request);
        
        /*@var $response Response */
        
        if (!$response->isSuccess()) {
            throw new \RunTimeException('Failed to get version page');
        }
        
        $text = strip_tags($response->getBody());
        
        if (preg_match('/Sources\sVersion\s([\d\.]+)/', $text, $m)) {
            return $m[1];
        }
        
        return null;
    }
    
    
    /**
     * Execute command when logged in
     */
    protected function userExecute(Client $client, InputInterface $input, OutputInterface $output)
    {
        $ovidentia = $this->getCoreVersion($client);
        if (isset($ovidentia)) {
            $output->writeln(str_pad($client->getUri(), 50).str_pad('Ovidentia', 30).$ovidentia);
        }
        
        $addons = $this->getIdx($client, 'list');
        $addons += $this->getIdx($client, 'theme');
        $addons += $this->getIdx($client, 'library');
        
        foreach ($addons as $name => $version) {
            $output->writeln(str_pad($client->getUri(), 50).str_pad($name, 30).$version);
        }
    }
}
