<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Ovish\Util;

use Ovidentia\Ovish\OviCommand\Command;
use Symfony\Component\Console\Output\OutputInterface;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Http\Cookies;
use Zend\Stdlib\Parameters;
use Zend\Uri\Http as HttpUri;

/**
 * Remote connexion to ovidentia
 *
 */
class Remote
{
    
    /**
     * @var string
     */
    private $uri = null;
    
    /**
     * @var string
     */
    private $nickname;
    
    /**
     * @var string
     */
    private $password;
    
    /**
     * @var \Zend\Http\Cookies
     */
    private $cookies;
    
    
    public function __construct($uri)
    {
        $this->uri = $uri;
        
        $this->cookies = new Cookies();
    }
    
    
    public function getUri()
    {
        return $this->uri;
    }
    
    
    /**
     * Set credentials from another remote object
     */
    public function setCredRemote(Remote $remote)
    {
        $this->nickname = $remote->nickname;
        $this->password = $remote->password;
    }
    
    
    /**
     *
     *
     */
    public function setCredentials($nickname, $password)
    {

        
        $this->nickname = $nickname;
        $this->password = $password;
    }
    
    
    
    protected function getClientOptions()
    {
        return array(
            'timeout' => 43200, // 12H
        //  'adapter' => 'Zend\Http\Client\Adapter\Socket',
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'sslverifypeer' => false,
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => false)
        );
    }
    

    
    /**
     * Get HTTP client for anonymous connexion
     * @return \Zend\Http\Client
     */
    public function getAnonymousClient()
    {

        $client = new Client();
        $client->setUri($this->uri);
        $client->setOptions($this->getClientOptions());
        
        return $client;
    }
    
    

    
    
    /**
     * Get HTTP client for user logged in connexion
     * @return \Zend\Http\Client
     */
    public function getUserClient()
    {
        $client = new Client();
        $client->setUri($this->uri);
        $client->setOptions($this->getClientOptions());
        
        $cookies = $this->cookies->getMatchingCookies($client->getUri());
        
        if (empty($cookies)) {
            if (!$this->login()) {
                return null;
            }
            
            $cookies = $this->cookies->getMatchingCookies($client->getUri());
            
            if (empty($cookies)) {
                throw new \RuntimeException('Login failed');
            }
        }
        
        if (!is_array($cookies)) {
            throw new \RuntimeException('Failed to get session cookie');
        }

        foreach ($cookies as $cookie) {
            $client->addCookie($cookie);
        }
        
        return $client;
    }
    
    /**
     * POST login informations and set session cookie
     */
    protected function login()
    {
        
        $client = $this->getAnonymousClient();
        
        $csrfToken = new CsrfToken();
        $token = $csrfToken->get(
            $client,
            array(
                'tg' => 'login',
                'cmd' => 'authform',
                'sAuthType' => 'Ovidentia'
            )
        );
        
        $client->setEncType(Client::ENC_FORMDATA);

        
        $request = new Request();
        $request->setUri($client->getUri());
        $request->setMethod(Request::METHOD_POST);
        
        $post = new Parameters();
        $post->fromArray(
            array(
                'tg'  => 'login',
                'sAuthType' => 'Ovidentia',
                'login' => 'login',
                'nickname' => $this->nickname,
                'password' => $this->password,
                'babCsrfProtect' => $token
            )
        );
        
        $request->setPost($post);
        $response = $client->send($request);
        $client->resetParameters();
        

        if (!$client->getCookies()) {
            throw new \RuntimeException('No authentication cookie set by authentication');
        }
        
        foreach ($client->getCookies() as $clientCookie) {
            $this->cookies->addCookie($clientCookie, $client->getUri());
        }

        return $response->isSuccess();
    }
    
    
    
    /**
     * Ask for nickname/password in command to login
     *
     * @param Command $command
     *
     * @return Client
     */
    public function loginOnCommand(Command $command, OutputInterface $output)
    {
        $dialog = $command->getHelperSet()->get('dialog');
        $uri = new HttpUri($this->uri);
        
        if (!isset($this->nickname)) {
            if (null === $nickname = $uri->getUser()) {
                $nickname = $dialog->ask(
                    $output,
                    $this->uri.' nickname:'
                );
            } else {
                // bizarement, l'identifiant n'est pas urldecode
                // on le decode quand meme pour pouvoir gerer le @ dans l'identifiant par exemple
                $nickname = urldecode($nickname);
            }
            
            $this->nickname = $nickname;
        }
        
        
        if (!isset($this->password)) {
            if (null === $password = $uri->getPassword()) {
                $password = $dialog->askHiddenResponse(
                    $output,
                    $this->uri.' password:'
                );
            } else {
                // bizarement, le mot de passe n'est pas urldecode
                // on le decode quand meme pour pouvoir gerer le @ dans le mot de passe par exemple
                $password = urldecode($password);
            }
            
            $this->password = $password;
        }

        
        $client = $this->getUserClient();
        
        return $client;
    }
    
    
    
    /**
     * Call remote command to logout
     * remove session information in class
     *
     */
    protected function logout()
    {
        $client = $this->getUserClient();
        
        $client->setParameterGet(
            array(
                'tg'  => 'login',
                'cmd' => 'signoff'
            )
        );
        
        $request = new Request();
        $request->setUri($client->getUri());
        $request->setMethod(Request::METHOD_GET);

        $response = $client->dispatch($request);

        $this->nickname = null;
        $this->password = null;
        
        return $response->isSuccess();
    }
}
