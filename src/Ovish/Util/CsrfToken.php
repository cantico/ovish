<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Ovish\Util;

use Sunra\PhpSimple\HtmlDomParser;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Stdlib\Parameters;

class CsrfToken
{
    /**
     * Get the CSRF token
     * @param Client $client
     * @param array $params
     * @return string
     */
    public function get(Client $client, Array $params)
    {
        $parameters = new Parameters();
        $parameters->fromArray($params);
    
        $request = new Request();
        $request->setUri($client->getUri());
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery($parameters);
    
        $response = $client->send($request);
    
        if (!$response->isSuccess()) {
            throw new \RunTimeException('Failed to get CSRF token from '.print_r($params, true));
        }
    
        $body = $response->getBody();
        $dom = HtmlDomParser::str_get_html($body);
    
        if (!isset($dom) || !$dom) {
            throw new \RuntimeException('Failed to get HTML from the response');
        }
    
        $input = $dom->find('input[name=babCsrfProtect]', 0);
    
        if (!$input) {
            // This is probably an old ovidentia, the csrf token validation will not be done
            return '';
            //throw new \RuntimeException('input[name=babCsrfProtect] not found in html');
        }
    
        $token = $input->value;
        /*
        if (empty($token)) {
            throw new \RuntimeException('Failed to get CSRF token, empty value');
        }
        */
        
        $client->resetParameters();
        
        return $token;
    }
}
