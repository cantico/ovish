<?php

namespace Ovidentia\Ovish\Tests;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Ovidentia\Ovish\OviCommand\InstallCommand;

class InstallCommandTest extends \PHPUnit_Framework_TestCase
{
    

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage test.zip
     */
    public function testExecute()
    {
        $application = new Application();
        $application->add(new InstallCommand());
    
        $command = $application->find('install');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            array(
                'command' => $command->getName(),
                '--remote' => 'http://ovish:012345678@localhost:3003',
                'archive' => 'test.zip'
            )
        );
    
        $output = $commandTester->getDisplay();
    }

}
