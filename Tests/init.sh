#!/bin/bash

mysql -u root -e 'drop database test;'
mysql -u root -e 'create database test;'
mysql -u root -e 'GRANT USAGE ON test.* TO "test"@"%";'


set -e

./vendor/bin/phpcs --standard=PSR2 src/

cd vendor/ovidentia/ovidentia

if [ -f "install.old" ]
then
	mv install.old install.php
fi

php install.php --dbname test --dblogin test --dbdrop --ulpath /tmp
mysql -u root test -e "update bab_users SET nickname='ovish', confirm_hash='8ee36f2ce606ef6af71335244dfa2d63' WHERE nickname='admin@admin.bab';"

php -d date.timezone=Europe/Paris -S localhost:3003 &
SERVER_PID=$!
sleep 5

# need a query on homepage to exec bab_newInstall()
wget http://localhost:3003?tg=login

set +e

cd ../../..
phpunit -d date.timezone=Europe/Paris --bootstrap vendor/autoload.php --coverage-clover=coverage.clover
kill $SERVER_PID

set -e

wget --no-check-certificate https://scrutinizer-ci.com/ocular.phar
php ocular.phar code-coverage:upload --format=php-clover coverage.clover

