<?php

namespace Ovidentia\Ovish\Tests;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Ovidentia\Ovish\OviCommand\ListCommand;
use Ovidentia\Ovish\Application as Ovish;

class ListCommandTest extends \PHPUnit_Framework_TestCase
{
    
    public function testExecute()
    {
        $application = new Ovish();
        $application->add(new ListCommand());
    
        $command = $application->find('rlist');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            array(
                'command' => $command->getName(),
                '--remote' => 'http://ovish:012345678@localhost:3003'
            )
        );
    
        $output = $commandTester->getDisplay();
        
        $output = trim($output);
        
        // ovidentia only, no addons
        $this->assertRegExp('/Ovidentia\s+\d+\.\d+\.\d+$/',$output);
    }
    

}
